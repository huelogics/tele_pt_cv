from flask import Flask, request, jsonify
import json
import base64
import cv2
import numpy as np
from img_process import process  # Assuming this imports correctly

app = Flask(__name__)

@app.route('/process_image', methods=['POST'])
def process_image():
    try:
        data = request.get_json()
        frame = data['frame']
        joint = data['joint']
        processed_img = process(frame, joint)  # Process the image
    
        return processed_img
        # Decode the processed image from base64 and save
        '''img_data = base64.b64decode(json.loads(processed_img)['array'])
        nparr = np.frombuffer(img_data, np.uint8)
        img = cv2.imdecode(nparr, cv2.IMREAD_ANYCOLOR)
        cv2.imwrite('savedImage.jpg', img)
        
        return jsonify({'message': 'Image processed and saved successfully.'})'''
    except Exception as e:
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True, port=3300)

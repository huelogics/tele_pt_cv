import cv2
import numpy as np
import json
import base64
import mediapipe as mp
import logging

# Initialize MediaPipe Pose.
mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose
pose = mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5)

# Initialize logging
logging.basicConfig(level=logging.INFO)

# Function to calculate angles using 2D coordinates.
def calculate_angle_2d(a, b, c):
    a = np.array(a[:2])  # First point
    b = np.array(b[:2])  # Mid point
    c = np.array(c[:2])  # End point
    
    radians = np.arctan2(c[1] - b[1], c[0] - b[0]) - np.arctan2(a[1] - b[1], a[0] - b[0])
    angle = np.abs(radians * 180.0 / np.pi)
    
    if angle > 180.0:
        angle = 360 - angle
    
    return round(angle, 2)

# Function to calculate angles using 3D coordinates.
def calculate_angle_3d(a, b, c):
    a = np.array(a)  # First point (x, y, z)
    b = np.array(b)  # Mid point (x, y, z)
    c = np.array(c)  # End point (x, y, z)
    
    ba = a - b  # vector from b to a
    bc = c - b  # vector from b to c    

    cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))
    angle = np.arccos(cosine_angle)
    
    angle = np.degrees(angle)
    return round(angle, 2)

# Function to calculate derived points.
def calculate_derived_points(joint, landmarks):
    if joint in ['Right_Shoulder_Internal_Rotation', 'Left_Shoulder_Internal_Rotation', 'Right_Shoulder_External_Rotation', 'Left_Shoulder_External_Rotation']:
        elbow = landmarks[mp_pose.PoseLandmark.RIGHT_ELBOW.value] if 'Right' in joint else landmarks[mp_pose.PoseLandmark.LEFT_ELBOW.value]
        wrist = landmarks[mp_pose.PoseLandmark.RIGHT_WRIST.value] if 'Right' in joint else landmarks[mp_pose.PoseLandmark.LEFT_WRIST.value]
        length = np.sqrt((wrist.x - elbow.x)**2 + (wrist.y - elbow.y)**2)
        initial_wrist = [elbow.x, elbow.y - length]
        return [initial_wrist, [elbow.x, elbow.y], [wrist.x, wrist.y]]
    elif joint in ['Right_Forearm_Supination', 'Left_Forearm_Supination', 'Right_Forearm_Pronation', 'Left_Forearm_Pronation']:
        wrist = landmarks[mp_pose.PoseLandmark.RIGHT_WRIST.value] if 'Right' in joint else landmarks[mp_pose.PoseLandmark.LEFT_WRIST.value]
        thumb = landmarks[mp_pose.PoseLandmark.RIGHT_THUMB.value] if 'Right' in joint else landmarks[mp_pose.PoseLandmark.LEFT_THUMB.value]
        length = np.sqrt((thumb.x - wrist.x)**2 + (thumb.y - wrist.y)**2)
        initial_thumb = [wrist.x, wrist.y - length]
        return [initial_thumb, [wrist.x, wrist.y], [thumb.x, thumb.y]]
    elif joint in ['Right_Hip_External_Rotation', 'Left_Hip_External_Rotation', 'Right_Hip_Internal_Rotation', 'Left_Hip_Internal_Rotation']:
        knee = landmarks[mp_pose.PoseLandmark.RIGHT_KNEE.value] if 'Right' in joint else landmarks[mp_pose.PoseLandmark.LEFT_KNEE.value]
        ankle = landmarks[mp_pose.PoseLandmark.RIGHT_ANKLE.value] if 'Right' in joint else landmarks[mp_pose.PoseLandmark.LEFT_ANKLE.value]
        length = np.sqrt((ankle.x - knee.x)**2 + (ankle.y - knee.y)**2)
        initial_ankle = [knee.x, knee.y + length]
        return [initial_ankle, [knee.x, knee.y], [ankle.x, ankle.y]]
    elif joint in ['Right_Ankle_Plantarflexion', 'Left_Ankle_Plantarflexion']:
        return None
    else:
        return None

# Function to process the image frame and detect the angle at the specified joint.
def process(frame, joint):
    try:
        logging.info("Starting process function.")
        
        # Decode base64 image.
        image = np.frombuffer(base64.b64decode(frame), np.uint8)
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        
        logging.info("Image decoded and read.")

        # Convert image to RGB.
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image.flags.writeable = False
        
        # Perform pose detection.
        results = pose.process(image)
        
        logging.info("Pose detection complete.")

        if not results.pose_landmarks:
            logging.error("No pose landmarks detected.")
            raise ValueError('No pose landmarks detected.')

        landmarks = results.pose_landmarks.landmark
        
        # Convert back to BGR for OpenCV.
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        # Mapping from joint names to MediaPipe Pose landmarks.
        joint_landmarks = {
            'Right_Ankle_Inversion': [mp_pose.PoseLandmark.RIGHT_KNEE, mp_pose.PoseLandmark.RIGHT_ANKLE, mp_pose.PoseLandmark.RIGHT_HEEL], # 3D
            'Left_Ankle_Inversion': [mp_pose.PoseLandmark.LEFT_KNEE, mp_pose.PoseLandmark.LEFT_ANKLE, mp_pose.PoseLandmark.LEFT_HEEL], # 3D
            'Right_Ankle_Eversion': [mp_pose.PoseLandmark.RIGHT_KNEE, mp_pose.PoseLandmark.RIGHT_ANKLE, mp_pose.PoseLandmark.RIGHT_HEEL], # 3D
            'Left_Ankle_Eversion': [mp_pose.PoseLandmark.LEFT_KNEE, mp_pose.PoseLandmark.LEFT_ANKLE, mp_pose.PoseLandmark.LEFT_HEEL], # 3D
            'Right_Ankle_Dorsiflexion': [mp_pose.PoseLandmark.RIGHT_KNEE, mp_pose.PoseLandmark.RIGHT_ANKLE, mp_pose.PoseLandmark.RIGHT_FOOT_INDEX], # 2D
            'Left_Ankle_Dorsiflexion': [mp_pose.PoseLandmark.LEFT_KNEE, mp_pose.PoseLandmark.LEFT_ANKLE, mp_pose.PoseLandmark.LEFT_FOOT_INDEX], # 2D
            'Right_Elbow_Extension': [mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.RIGHT_ELBOW, mp_pose.PoseLandmark.RIGHT_WRIST], # 2D
            'Left_Elbow_Extension': [mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.LEFT_ELBOW, mp_pose.PoseLandmark.LEFT_WRIST], # 2D            
            'Right_Elbow_Flexion': [mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.RIGHT_ELBOW, mp_pose.PoseLandmark.RIGHT_WRIST], # 2D
            'Left_Elbow_Flexion': [mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.LEFT_ELBOW, mp_pose.PoseLandmark.LEFT_WRIST], # 2D
            'Right_Wrist_Radial_Deviation': [mp_pose.PoseLandmark.RIGHT_ELBOW, mp_pose.PoseLandmark.RIGHT_WRIST, mp_pose.PoseLandmark.RIGHT_INDEX], # 3D
            'Left_Wrist_Radial_Deviation': [mp_pose.PoseLandmark.LEFT_ELBOW, mp_pose.PoseLandmark.LEFT_WRIST, mp_pose.PoseLandmark.LEFT_INDEX], # 3D
            'Right_Wrist_Ulnar_Deviation': [mp_pose.PoseLandmark.RIGHT_ELBOW, mp_pose.PoseLandmark.RIGHT_WRIST, mp_pose.PoseLandmark.RIGHT_INDEX], # 3D
            'Left_Wrist_Ulnar_Deviation': [mp_pose.PoseLandmark.LEFT_ELBOW, mp_pose.PoseLandmark.LEFT_WRIST, mp_pose.PoseLandmark.LEFT_INDEX], # 3D
            'Right_Wrist_Extension': [mp_pose.PoseLandmark.RIGHT_ELBOW, mp_pose.PoseLandmark.RIGHT_WRIST, mp_pose.PoseLandmark.RIGHT_PINKY], # 2D
            'Left_Wrist_Extension': [mp_pose.PoseLandmark.LEFT_ELBOW, mp_pose.PoseLandmark.LEFT_WRIST, mp_pose.PoseLandmark.LEFT_PINKY], # 2D
            'Right_Wrist_Flexion': [mp_pose.PoseLandmark.RIGHT_ELBOW, mp_pose.PoseLandmark.RIGHT_WRIST, mp_pose.PoseLandmark.RIGHT_PINKY], # 2D
            'Left_Wrist_Flexion': [mp_pose.PoseLandmark.LEFT_ELBOW, mp_pose.PoseLandmark.LEFT_WRIST, mp_pose.PoseLandmark.LEFT_PINKY], # 2D
            'Right_Knee_Extension': [mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.RIGHT_KNEE, mp_pose.PoseLandmark.RIGHT_ANKLE], # 2D
            'Left_Knee_Extension': [mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.LEFT_KNEE, mp_pose.PoseLandmark.LEFT_ANKLE], # 2D
            'Right_Knee_Flexion': [mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.RIGHT_KNEE, mp_pose.PoseLandmark.RIGHT_ANKLE], # 2D
            'Left_Knee_Flexion': [mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.LEFT_KNEE, mp_pose.PoseLandmark.LEFT_ANKLE], # 2D
            'Right_Hip_Abduction': [mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.RIGHT_KNEE], # 2D
            'Left_Hip_Abduction': [mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.LEFT_KNEE], # 2D
            'Right_Hip_Adduction': [mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.RIGHT_KNEE], # 2D
            'Left_Hip_Adduction': [mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.LEFT_KNEE], # 2D
            'Right_Hip_Extension': [mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.RIGHT_KNEE], # 2D
            'Left_Hip_Extension': [mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.LEFT_KNEE], # 2D
            'Right_Hip_Flexion': [mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.RIGHT_KNEE], # 2D
            'Left_Hip_Flexion': [mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.LEFT_KNEE], # 2D
            'Right_Shoulder_Abduction': [mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.RIGHT_ELBOW], # 3D
            'Left_Shoulder_Abduction': [mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.LEFT_ELBOW], # 3D
            'Right_Shoulder_Flexion': [mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.RIGHT_ELBOW], # 2D
            'Left_Shoulder_Flexion': [mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.LEFT_ELBOW], # 2D
            'Right_Shoulder_Horizontal_Abduction': [mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.RIGHT_ELBOW], # 3D
            'Left_Shoulder_Horizontal_Abduction': [mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.LEFT_ELBOW], # 3D
            'Right_Shoulder_Horizontal_Adduction': [mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.RIGHT_ELBOW], # 3D
            'Left_Shoulder_Horizontal_Adduction': [mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.LEFT_ELBOW], # 3D
            'Right_Shoulder_Extension': [mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.RIGHT_ELBOW], # 2D
            'Left_Shoulder_Extension': [mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.LEFT_ELBOW], # 2D
            'Right_Shoulder_Internal_Rotation': ['Derived_Point', mp_pose.PoseLandmark.RIGHT_ELBOW, mp_pose.PoseLandmark.RIGHT_WRIST], # 2D
            'Left_Shoulder_Internal_Rotation': ['Derived_Point', mp_pose.PoseLandmark.LEFT_ELBOW, mp_pose.PoseLandmark.LEFT_WRIST], # 2D
            'Right_Shoulder_External_Rotation': ['Derived_Point', mp_pose.PoseLandmark.RIGHT_ELBOW, mp_pose.PoseLandmark.RIGHT_WRIST], # 2D
            'Left_Shoulder_External_Rotation': ['Derived_Point', mp_pose.PoseLandmark.LEFT_ELBOW, mp_pose.PoseLandmark.LEFT_WRIST], # 2D
            'Right_Hip_External_Rotation': [mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.RIGHT_KNEE], # 2D
            'Left_Hip_External_Rotation': [mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.LEFT_KNEE], # 2D
            'Right_Hip_Internal_Rotation': [mp_pose.PoseLandmark.RIGHT_SHOULDER, mp_pose.PoseLandmark.RIGHT_HIP, mp_pose.PoseLandmark.RIGHT_KNEE], # 2D
            'Left_Hip_Internal_Rotation': [mp_pose.PoseLandmark.LEFT_SHOULDER, mp_pose.PoseLandmark.LEFT_HIP, mp_pose.PoseLandmark.LEFT_KNEE], # 2D
            'Right_Forearm_Supination': ['Derived_Point', mp_pose.PoseLandmark.RIGHT_WRIST, mp_pose.PoseLandmark.RIGHT_THUMB],
            'Left_Forearm_Supination': ['Derived_Point', mp_pose.PoseLandmark.LEFT_WRIST, mp_pose.PoseLandmark.LEFT_THUMB],
            'Right_Forearm_Pronation': ['Derived_Point', mp_pose.PoseLandmark.RIGHT_WRIST, mp_pose.PoseLandmark.RIGHT_THUMB],
            'Left_Forearm_Pronation': ['Derived_Point', mp_pose.PoseLandmark.LEFT_WRIST, mp_pose.PoseLandmark.LEFT_THUMB],

            # Ambiguous (Need to check since the foot pinky and index can sometimes bend)
            'Right_Ankle_Plantarflexion': [mp_pose.PoseLandmark.RIGHT_KNEE, mp_pose.PoseLandmark.RIGHT_ANKLE, mp_pose.PoseLandmark.RIGHT_FOOT_INDEX], # 2D
            'Left_Ankle_Plantarflexion': [mp_pose.PoseLandmark.LEFT_KNEE, mp_pose.PoseLandmark.LEFT_ANKLE, mp_pose.PoseLandmark.LEFT_FOOT_INDEX], # 2D
        }

        if joint not in joint_landmarks:
            logging.error(f"Joint name {joint} is not recognized.")
            raise ValueError(f"Joint name {joint} is not recognized.")

        # Retrieve landmark coordinates and draw them.
        point_coords = []  # To store the coordinates of the points to draw
        if 'Derived_Point' in joint_landmarks[joint]:
            derived_points = calculate_derived_points(joint, landmarks)
            if derived_points:
                point_coords.extend(derived_points)
            else:
                logging.error("Derived points calculation failed.")
                raise ValueError('Failed to calculate derived points.')
        else:
            for landmark in joint_landmarks[joint]:
                point = landmarks[landmark.value]
                x, y, z = point.x, point.y, point.z
                point_coords.append([x, y, z])
                cv2.circle(image, (int(x * image.shape[1]), int(y * image.shape[0])), 5, (255, 0, 0), thickness=-1)

        logging.info("Landmarks retrieved and drawn.")

        # Draw lines between landmarks.
        cv2.line(image, (int(point_coords[0][0] * image.shape[1]), int(point_coords[0][1] * image.shape[0])),
                 (int(point_coords[1][0] * image.shape[1]), int(point_coords[1][1] * image.shape[0])), (0, 255, 0), 2)
        cv2.line(image, (int(point_coords[1][0] * image.shape[1]), int(point_coords[1][1] * image.shape[0])),
                 (int(point_coords[2][0] * image.shape[1]), int(point_coords[2][1] * image.shape[0])), (0, 255, 0), 2)

        logging.info("Lines drawn between landmarks.")

        # Calculate the angle based on the required dimensions.
        if joint in ['Right_Ankle_Inversion', 'Left_Ankle_Inversion', 'Right_Ankle_Eversion', 'Left_Ankle_Eversion', 
                     'Right_Ankle_Plantarflexion', 'Left_Ankle_Plantarflexion',
                     'Right_Wrist_Radial_Deviation', 'Left_Wrist_Radial_Deviation', 'Right_Wrist_Ulnar_Deviation', 'Left_Wrist_Ulnar_Deviation',
                     'Right_Shoulder_Abduction', 'Left_Shoulder_Abduction', 'Right_Shoulder_Horizontal_Abduction', 'Left_Shoulder_Horizontal_Abduction']:
            angle = calculate_angle_3d(point_coords[0], point_coords[1], point_coords[2])
        elif joint in ['Right_Shoulder_Horizontal_Adduction', 'Left_Shoulder_Horizontal_Adduction']:
            angle = 180 - (calculate_angle_3d(point_coords[0], point_coords[1], point_coords[2]))
        else:
            angle = calculate_angle_2d(point_coords[0], point_coords[1], point_coords[2])

        # Visualize the angle.
        cv2.putText(image, f"Angle: {str(angle)}", (int(point_coords[1][0] * image.shape[1]) - 80, int(point_coords[1][1] * image.shape[0]) + 20),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 1)
        
        
        logging.info(f"Angle-> {angle}")
        logging.info("Angle visualized.")

        # Encode the resulting image to base64 and prepare it to be sent as JSON.
        retval, buffer = cv2.imencode('.jpg', image)
        jpg_as_text = base64.b64encode(buffer).decode('utf-8')
        
        logging.info("Image encoding complete.")
        
        return jpg_as_text
        #return json.dumps({"array": jpg_as_text})
    except Exception as e:
        logging.error(f"Error in process function: {e}")
        return json.dumps({"error": "Failed to process image"})


'''import cv2
import numpy as np
import json
import base64
import mediapipe as mp
import math
import logging

# Initialize MediaPipe Pose.
mp_drawing = mp.solutions.drawing_utils
mp_pose = mp.solutions.pose
pose = mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5)

# Initialize logging
logging.basicConfig(level=logging.INFO)

# Function to calculate the depth-based angle.
def depth_based_angle(a, b, c):
    try:
        # Differences in coordinates
        opposite = abs(a[1] - b[1])
        adjacent = abs(c[0] - a[0])
        depth = abs(a[2] - b[2])

        # Calculate the angle considering the 3D space with depth
        angle1 = math.degrees(math.atan(opposite / (adjacent + depth)))
        angle1 = round(angle1, 2)
        return angle1
    except Exception as e:
        logging.error(f"Error in depth_based_angle: {e}")
        raise

# Function to process the image frame and detect the 3D angle at the specified joint.
def process(frame, joint):
    try:
        logging.info("Starting process function.")
        
        # Decode base64 image.
        image = np.frombuffer(base64.b64decode(frame), np.uint8)
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        
        logging.info("Image decoded and read.")

        # Convert image to RGB.
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image.flags.writeable = False
        
        # Perform pose detection.
        results = pose.process(image)
        
        logging.info("Pose detection complete.")

        if not results.pose_landmarks:
            logging.error("No pose landmarks detected.")
            raise ValueError('No pose landmarks detected.')

        landmarks = results.pose_landmarks.landmark
        
        # Convert back to BGR for OpenCV.
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        # Mapping from joint names to MediaPipe Pose landmarks.
        joint_landmarks = {
            'Right_Ankle_Inversion': [mp_pose.PoseLandmark.RIGHT_KNEE, mp_pose.PoseLandmark.RIGHT_ANKLE, mp_pose.PoseLandmark.RIGHT_HEEL],
        }

        if joint not in joint_landmarks:
            logging.error(f"Joint name {joint} is not recognized.")
            raise ValueError(f"Joint name {joint} is not recognized.")

        # Retrieve landmark coordinates and draw them.
        point_coords = []  # To store the coordinates of the points to draw
        for landmark in joint_landmarks[joint]:
            point = landmarks[landmark.value]
            x, y, z = point.x, point.y, point.z
            point_coords.append([x, y, z])
            cv2.circle(image, (int(x * image.shape[1]), int(y * image.shape[0])), 5, (255, 0, 0), thickness=-1)

        logging.info("Landmarks retrieved and drawn.")

        # Draw lines between landmarks.
        cv2.line(image, (int(point_coords[0][0] * image.shape[1]), int(point_coords[0][1] * image.shape[0])),
                 (int(point_coords[1][0] * image.shape[1]), int(point_coords[1][1] * image.shape[0])), (0, 255, 0), 2)
        cv2.line(image, (int(point_coords[1][0] * image.shape[1]), int(point_coords[1][1] * image.shape[0])),
                 (int(point_coords[2][0] * image.shape[1]), int(point_coords[2][1] * image.shape[0])), (0, 255, 0), 2)

        logging.info("Lines drawn between landmarks.")

        # Calculate the depth-based angle.
        angle_depth = depth_based_angle(point_coords[0], point_coords[1], point_coords[2])

        # Visualize the angle.
        cv2.putText(image, f"Depth Angle: {str(angle_depth)}", (int(point_coords[1][0] * image.shape[1]), int(point_coords[1][1] * image.shape[0]) + 20),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)

        logging.info("Angle visualized.")

        # Encode the resulting image to base64 and prepare it to be sent as JSON.
        retval, buffer = cv2.imencode('.jpg', image)
        jpg_as_text = base64.b64encode(buffer).decode('utf-8')
        
        logging.info("Image encoding complete.")
        
        return json.dumps({"array": jpg_as_text})
    except Exception as e:
        logging.error(f"Error in process function: {e}")
        return json.dumps({"error": "Failed to process image"})'''